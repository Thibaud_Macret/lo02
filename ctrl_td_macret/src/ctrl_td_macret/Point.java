package ctrl_td_macret;

public class Point {
	protected double x;
	protected double y;
	protected String nom;
	
	public Point(String nm, double x, double y) {
		this.x = x;
		this.y = y;
		this.nom = nm;
	}
	
	public String toString() {
		return ("Le point " + this.nom + " a pour coordonées (" + this.x + "," + this.y + ").");
	}
	
}

package ctrl_td_macret;

public class Main {
	
	public static void main(String[] args) {
		DistanceOrthdromique m0 = new DistanceOrthdromique();
		Itineraire sealand2LeCap = new Itineraire(m0);
		sealand2LeCap.ajouterEtape(new Aeroport("Sealand Republic", "SRU", 51.890000, 1.476200));
		sealand2LeCap.ajouterEtape(new Aeroport("Paris", "CDG", 49.00979, 2.54689));
		sealand2LeCap.ajouterEtape(new Aeroport("New York", "JFK", 40.64457, -73.78200));
		sealand2LeCap.ajouterEtape(new Aeroport("Tokyo", "KIX", 34.43264, 135.23009));
		sealand2LeCap.ajouterEtape(new Aeroport("CapeTown", "CPT", -33.97168, 18.60200));
		sealand2LeCap.afficherTrajetMieux();
	}
}

package ctrl_td_macret;

public class DistanceOrthdromique implements Distanceable{

	@Override
	public double calculerDistance(Point depart, Point arrive) {
		return (6371 * Math.acos(Math.sin(Math.toRadians(depart.x)) * Math.sin(Math.toRadians(arrive.x)) + 
				Math.cos(Math.toRadians(depart.x)) * Math.cos(Math.toRadians(arrive.x)) * Math.cos(Math.toRadians(arrive.y) -
				Math.toRadians(depart.y))) );
	}

}

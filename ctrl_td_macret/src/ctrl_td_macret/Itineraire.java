package ctrl_td_macret;

import java.util.ArrayList;
import java.util.List;

public class Itineraire {
	private List<Point> listEtapes;
	private Distanceable calculateur;
	
	public Itineraire(Distanceable calculateur) {
		listEtapes = new ArrayList<Point>();
		this.calculateur = calculateur;
	}
	
	public void ajouterEtape(Point p) {
		this.listEtapes.add(p);
	}
	
	public double calculerDistanceIttineraire(){
		double total = 0;
		for(int index = 1; index<listEtapes.size(); index++) {
			total += (calculateur.calculerDistance(listEtapes.get(index-1), listEtapes.get(index)));
		}
		return total;
	}
	
	public void afficherTrajet() {
		System.out.println("Voici les �tapes de ce trajet :");
		for(Point etape : listEtapes) {
			System.out.println(etape.toString());
		}
		System.out.println("La distance totale est de " + calculerDistanceIttineraire() + " km.");
	}
	
	//bonus
	
	public double calculerDistanceEtape(int indexDep) {
		if(indexDep >= listEtapes.size() || indexDep < 0) {return 0;}
		return (calculateur.calculerDistance(listEtapes.get(indexDep), listEtapes.get(indexDep+1)));
	}
	
	public void afficherTrajetMieux() {
		System.out.println("Voici les �tapes de ce trajet :");
		for(int index = 0; index<listEtapes.size()-1; index++) {
			System.out.println("Etape " + index + " : " + listEtapes.get(index).toString() + " - " + listEtapes.get(index+1).toString() +
			" ditance parcourue : " + calculerDistanceEtape(index));
		}
		System.out.println("La distance totale est de " + calculerDistanceIttineraire() + " km.");
	}
}

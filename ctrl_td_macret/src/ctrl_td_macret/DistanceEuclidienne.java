package ctrl_td_macret;

public class DistanceEuclidienne implements Distanceable{

	@Override
	public double calculerDistance(Point depart, Point arrive) {
		return Math.sqrt(Math.pow((arrive.x - depart.x), 2) + Math.pow((arrive.y - depart.y), 2));
	}

}

package ctrl_td_macret;

public interface Distanceable {
	public double calculerDistance(Point depart, Point arrive);
}

package ctrl_td_macret;

public class Aeroport extends Point{
	private String codeIATA;
	
	public Aeroport(String nm, String ic, double x, double y) {
		super(nm, x, y);
		this.codeIATA = ic;
	}

	public String toString() {
		return("L'a�roport" + this.nom + " de code IATA " + this.codeIATA + ", a pour coordon�es (" + this.x + "," + this.y + ").");
	}
}

package correction;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class VueCompteur implements Observer {

	Compteur counter;
	JFrame panneau;
	JButton lancer;
	JButton stopper;
	JTextField infoCompteur;
	
	public VueCompteur(Compteur c){
		
		this.counter = c;
		this.counter.addObserver(this);
		
		panneau = new JFrame("Test du compteur");
		lancer = new JButton("Lancer !!!");
		lancer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				if(counter.getValue() <= 0){
					counter.compter();
				}else{
					counter.demarrer();
				}
				
				
			}
		});
		
		stopper = new JButton("Stop !!!");
		stopper.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				counter.stopper();
			}
		});
		
		infoCompteur = new JTextField("Lancez le compteur :)");
	
		Container reservoir = panneau.getContentPane();
		reservoir.setLayout(new BorderLayout());
		reservoir.add(infoCompteur, BorderLayout.NORTH);
		reservoir.add(lancer, BorderLayout.SOUTH);
		reservoir.add(stopper, BorderLayout.WEST);
		panneau.pack();
		panneau.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panneau.setVisible(true);
		
	}
	
	public void update(Observable o, Object msg){
		this.infoCompteur.setText("Compteur : "+counter.getValue());
	}
	
	public static void main(String args[]){
		Compteur c = new Compteur();
		VueCompteur vc = new VueCompteur(c);		
	}
	
}

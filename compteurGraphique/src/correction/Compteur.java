package correction;

import java.util.Observable;

@SuppressWarnings("deprecation")
public class Compteur extends Observable implements Runnable{

	private static final int TEMPO = 200;
	private int counter;
	private boolean estActif = false;
	private Thread t;
	
	public Compteur(){
		this.t = new Thread(this);
		this.counter = 0;
	}
	
	public int getValue(){
		return this.counter;
	}
	
	public boolean isActive(){
		return this.estActif;
	}
	
	public void demarrer(){
		this.estActif = true;
		System.out.println("Actif : "+this.estActif);
	}
	
	public void stopper(){
		this.estActif = false;
		System.out.println("Actif : "+this.estActif);
	}
	
	public void run(){
		while(true){
			if(this.estActif){
				this.counter++;
				this.setChanged();
				this.notifyObservers();
				System.out.println(this.getValue());
				this.pause();
			}else{
				//System.out.println("Don't kill my thread !!");
				this.pause();
			}
			
			
		}
	}
	
	public void compter(){
		this.estActif = true;
		this.t.start();
	}
	
	public void pause(){
		try{
			Thread.sleep(Compteur.TEMPO);
		}catch(InterruptedException e){
			e.printStackTrace();
		}
	}
	

	
	
}

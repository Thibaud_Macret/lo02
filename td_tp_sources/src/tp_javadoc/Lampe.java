package tp_javadoc;

public class Lampe {

    public final static int PUISSANCE_STANDARD = 100;
    public int puissance;
    public Boolean allumee;
    
    public Lampe(int puissance) {
    	this.puissance = puissance;
    	this.allumee = false;
    }

    public int getPuissance() {
    	return this.puissance;
    }

    public boolean isAllumee() {
    	return this.allumee;
    }

    public void allumer() {
    	this.allumee = true;
    }
    
    public void eteindre() {
    	this.allumee = false;
    }

}

package td_threads;

public class Voiture extends Thread{

	public static final int TEMPORISATION = 1000;
	public final int capaciteReservoir = 50;
	public final double consommation = 0.1;
	protected double essence;
	protected boolean roule;

	public Voiture() {
		this.essence = capaciteReservoir;
		this.roule = false;
	}


	public String toString() {
		StringBuffer sb = new StringBuffer ("Le vehicule ");
		if (this.roule == false) {
			sb.append("est a l'arret. ");
		} else {
			sb.append("roule encore. ");
		}
		sb.append("Il reste ");
		sb.append(this.essence);
		sb.append(" litres d'essences dans son reservoir.");
		return sb.toString();
	}
	
	public void demarrer() {
		this.roule = true;
		this.start();
	}


	public void stopper() {
		this.roule = false;
	}

	public void run() {
		System.out.println("Le vehicule d�marre...");
		while (this.roule && this.essence > 0) {
			this.essence -= this.consommation;
			try {
			    Thread.sleep(Voiture.TEMPORISATION);
			} catch (InterruptedException e) {}
			System.out.println(this);
		}
		System.out.println("Le v�hicule est arrete.");
	}
}

package td_threads;

public class main {
	public static void main(String[] args) {
		Voiture v1 = new Voiture();
		Voiture v2 = new Voiture();
		Voiture v3 = new Voiture();
		v1.demarrer();
		v2.demarrer();
		v3.demarrer();
		
		try{
			Thread.sleep((long)(5000));
		} catch(InterruptedException e) {e.printStackTrace();} 
		
		v1.stopper();
		v2.stopper();
		v3.stopper();
	}
}

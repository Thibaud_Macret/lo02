package td_exceptions;

public class Main {
	public static void main(String[] args) {
		//PARTIE 1
		PolynomeDegreDeux polyException = new PolynomeDegreDeux(0, 0, 0);
		try {
			System.out.println(polyException.getRoots());
		} catch (InvalidPolynomException e) {
			System.out.println("MESSAGE :");
			System.out.println(e.getMessage());
			System.out.println("STACK TRACE :");
			e.printStackTrace();
		}
		//PARTIE 2
		Interrupteur inter = new Interrupteur();
		for (int i = 0; i < 5; i++) {
			 inter.appuyer();
			 System.out.println(inter);
		}
	}
}

package td_exceptions;

public class CannotBeOffException extends Exception {
	public CannotBeOffException(String message) {
		super(message);
	}
}

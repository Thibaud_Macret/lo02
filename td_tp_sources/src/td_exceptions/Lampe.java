package td_exceptions;

public class Lampe {
	public final static int PUISSANCE_STANDARD = 100;

	private int puissance;
	private boolean allumee;
	private int nbActivations = 1; 

	public Lampe(int puissance) {
		this.puissance = puissance;
		allumee = false;
	}
	
	public int getPuissance() {
		return puissance;
	}
	
	public boolean isAllumee() {
		return allumee;
	}
	
	public void allumer() throws CannotBeLitException{
		if(allumee) {throw new CannotBeLitException("La est d�j� allum�e !");}
		if(nbActivations==0) {throw new CannotBeLitException("La lampe est H.S.");}
		this.nbActivations--;
		this.allumee = true;
	}

	 public void eteindre() throws CannotBeOffException{
		if(!allumee) {throw new CannotBeOffException("La lampe est d�j� �teinte !");}
		this.allumee = false;
	 }

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Lampe (");
		sb.append("Puissance : " + puissance);
		sb.append("\t");
		sb.append("Allum�e : " + allumee);
		sb.append("\t");
		sb.append("NbActivation : " + nbActivations);
		sb.append(")\n");
		return sb.toString();
	}
}

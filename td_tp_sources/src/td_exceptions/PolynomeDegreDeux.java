package td_exceptions;

public class PolynomeDegreDeux {
	// x2, x1, x0
	private int[] polynome;
	
	public PolynomeDegreDeux(int x2, int x1, int x0) {
		polynome = new int[3];
		polynome[0] = x2;
		polynome[1] = x1;
		polynome[2] = x0;
	}
	 
	public double delta() {
		return polynome[1] * polynome[1] - 4 * polynome[0] * polynome[2];
	}
	
	public boolean hasComplexRoots() {
		return (this.delta() < 0);
	}
	
	public boolean hasDoubleRoot() {
		return (this.delta() == 0);
	}
	
	public boolean hasRealRoots() {
		return (this.delta() > 0);
	}
	
	private double moinsBSur2A() throws ArithmeticException {
		return -polynome[1] / (2 * polynome[0]);
	}
	
	private double racineDeltaSur2A () throws ArithmeticException {
		return Math.sqrt(Math.abs(this.delta())) / (2 * polynome[0]);
	}
	
	public int getX2() {
		return polynome[0];
	}
	
	public int getX1() {
		return polynome[1];
	}
	 
	public int getX0() {
		return polynome[2];
	}
	
	public boolean equals(Object o) {
		if (o instanceof PolynomeDegreDeux) {
			PolynomeDegreDeux p = (PolynomeDegreDeux)o;
			return ((p.getX2() == this.getX2()) & (p.getX1() == this.getX1()) & (p.getX0() == this.getX0()));
		} else {
			return false;
		}
	}
	
	public Complexe[] getRoots() throws InvalidPolynomException {
		Complexe[] racines;
		if (this.hasDoubleRoot()) {
			racines = new Complexe[1];
			try{racines[0] = new Complexe (this.moinsBSur2A(), 0);}
			catch(ArithmeticException e) {throw new InvalidPolynomException("test message exception", e);}
		} else {
			racines = new Complexe[2];
			if (this.hasRealRoots()) {
				try{racines[0] = new Complexe(this.moinsBSur2A() + this.racineDeltaSur2A(), 0);}
				catch(ArithmeticException e) {throw new InvalidPolynomException("test message exception", e);}
				try{racines[1] = new Complexe(this.moinsBSur2A() - this.racineDeltaSur2A(), 0);}
				catch(ArithmeticException e) {throw new InvalidPolynomException("test message exception",e);}
			} else {
				try{racines[0] = new Complexe(this.moinsBSur2A(), this.racineDeltaSur2A());}
				catch(ArithmeticException e) {throw new InvalidPolynomException("test message exception", e);}
				racines[1] = racines[0].conjugue();
			}
		}
		return racines;
	}
		 
	public String toString() {
		return new String(polynome[0] + "x2 + " + polynome[1] + "x + " + polynome[2]);
	}

}

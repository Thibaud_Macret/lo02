package td_exceptions;

public class CannotBeLitException extends Exception {
	public CannotBeLitException(String message) {
		super(message);
	}
}

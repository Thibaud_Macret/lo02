package td_exceptions;

public class Commutateur {
	 private Lampe[] lampes;
	 private int etat;
	 private int nombreEtats;
	 public Commutateur (int nombreLampes) {
		etat = 0;
		nombreEtats = nombreLampes * 2;
		lampes = new Lampe[nombreLampes];
		Lampe lampeFaible = new Lampe (Lampe.PUISSANCE_STANDARD / 2);
		lampes[0] = lampeFaible;
		for (int i = 1; i < nombreLampes; i++) {
			 Lampe lampeStandard = new Lampe (Lampe.PUISSANCE_STANDARD);
			 lampes[i] = lampeStandard;
		}
	}
	 
	public void commuter() throws InterrupteurMuda {
		etat = (etat + 1) % nombreEtats;
		int puissanceRequise = etat * Lampe.PUISSANCE_STANDARD / 2;
		// Allumage/eteignage de la lampe faible
		Lampe lampeFaible = lampes[0];
		if ((puissanceRequise % Lampe.PUISSANCE_STANDARD) != 0) {
			try {
				lampeFaible.allumer();
			} catch (CannotBeLitException e) {
				throw (new InterrupteurMuda(e));
			}
		} else {
			try {
				lampeFaible.eteindre();
			} catch (CannotBeOffException e) {
				throw (new InterrupteurMuda(e));
			}
		}
		// Allumage/Eteignage des autres lampes
		for (int i = 1; i <= (puissanceRequise / Lampe.PUISSANCE_STANDARD); i++) {
			 Lampe lampe = lampes[i];
			 try {
				lampe.allumer();
			} catch (CannotBeLitException e) {
				throw (new InterrupteurMuda(e));
			}
		}
		for (int i = Math.max((puissanceRequise / Lampe.PUISSANCE_STANDARD) + 1, 1); i < lampes.length; i++) {
			 Lampe lampe = lampes[i];
			 try {
				lampe.eteindre();
			} catch (CannotBeOffException e) {
				throw (new InterrupteurMuda(e));
			}
		}
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Commutateur (");
		sb.append("Etat : " + etat);
		sb.append(")\n");
		for (int i = 0; i < lampes.length; i++) {
			sb.append(lampes[i]);
		}
		return sb.toString();
	}
}

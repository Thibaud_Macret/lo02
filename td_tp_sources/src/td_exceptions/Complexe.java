package td_exceptions;

public class Complexe {
	private double partieReelle;
	private double partieImaginaire;
	 
	public Complexe (double pr, double pi) {
		this.partieReelle = pr;
		this.partieImaginaire = pi;
	}

	public double module() {
		return Math.sqrt(Math.pow(this.partieReelle, 2) +
		Math.pow(this.partieImaginaire, 2));
	}
	
	public void ajouter (Complexe c) {
		this.partieReelle += c.partieReelle;
		this.partieImaginaire += c.partieImaginaire;
	}

	public void multiplier (Complexe c) {
		double pr = this.partieReelle * c.partieReelle -
		this.partieImaginaire * c.partieImaginaire;
		double pi = this.partieReelle * c.partieImaginaire +
		this.partieImaginaire * c.partieReelle;
		this.partieReelle = pr;
		this.partieImaginaire = pi;
	}

	public Complexe conjugue () {
		return new Complexe (this.partieReelle, -this.partieImaginaire);
	 }

	public void ajouterReference(Complexe c) {
		c.partieReelle += this.partieReelle;
		c.partieImaginaire += this.partieImaginaire;
	}

	public void ajouterReference(int pr, int pi) {
		pr += this.partieReelle;
		pi += this.partieImaginaire;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("(");
		sb.append(this.partieReelle);
		sb.append(" + ");
		sb.append(this.partieImaginaire);
		sb.append("i)");
		return sb.toString();
	}

}

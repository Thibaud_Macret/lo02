package td_exceptions;

public class Interrupteur {
	private Commutateur commutateur;
	
	public Interrupteur () {commutateur = new Commutateur(5);}
	
	public void appuyer() {
		try {
			commutateur.commuter();
		} catch (InterrupteurMuda e) {
			System.out.println("Muda muda !");
			e.printStackTrace();
		}
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Interrupteur\n");
		sb.append(commutateur);
		return sb.toString();
	}
	
	public static void main (String[] args) {
		Interrupteur inter = new Interrupteur();
		for (int i = 0; i < 20; i++) {
			 inter.appuyer();
			 System.out.println(inter);
		}
	}

}

package td_threads_2;

import td_threads.Voiture;

public class Producteur extends Thread{
	private String nom;
	private BoiteAuxLettres target;
	
	public Producteur(String nom, BoiteAuxLettres BAL) {
		this.nom = nom;
		this.target = BAL;
		this.start();
	}
	
	public void run() {
		while(this.isAlive()) {
			try {
			    Thread.sleep((long)(1000+Math.random()*10000));
			} catch (InterruptedException e) {}
			act();
		}
	}
	
	public void act() {
		System.out.println(this.nom + ", le producteur tente d'acc�der. ");
		target.deposerMessage("Bonjour, moi c'est " + this.nom);
		System.out.println(this.nom+ " a d�pos� son message !");
	}
}

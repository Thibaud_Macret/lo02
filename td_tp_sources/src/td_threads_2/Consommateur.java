package td_threads_2;

public class Consommateur extends Thread{
	private String nom;
	private BoiteAuxLettres target;
	
	public Consommateur(String nom, BoiteAuxLettres BAL) {
		this.nom = nom;
		this.target = BAL;
		this.start();
	}
	
	public void run() {
		while(this.isAlive()) {
			try {
			    Thread.sleep((long)(1000+Math.random()*10000));
			} catch (InterruptedException e) {}
			act();
		}
	}
	
	public void act() {
		System.out.println(this.nom + ", le consommateur tente d'acc�der. ");
		String resultat = target.lireMessage();
		System.out.println(this.nom + " lit : " + resultat);
	}
}

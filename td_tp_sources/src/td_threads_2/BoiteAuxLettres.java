package td_threads_2;

public class BoiteAuxLettres {
	private String message;
	
	public BoiteAuxLettres() {
		this.message = "";
	}
	
	public synchronized String lireMessage() {
		while(this.message=="") {
			System.out.println("Rien � lire !");
			try {this.wait();} catch (InterruptedException e) {e.printStackTrace();}
		}
		String message = this.message;
		this.message = "";
		this.notifyAll();
		return message;
	}
	
	public synchronized void deposerMessage(String message) {
		while(this.message!="") {
			System.out.println("BAL pleine !");
			try {this.wait();} catch (InterruptedException e) {e.printStackTrace();}
		}
		this.message = message;
		this.notifyAll();
	}
}

package td_collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class TestCollections {
	public static void main(String[] args) {
		
		System.out.println("SET");
		//Set = aucuns doublons ! et pas de get(rank)
		Set<Integer> set = new HashSet<Integer>();
		set.add(23);
		set.add(15);
		set.add(19);
		set.add(19);
		
		System.out.println(set.contains(15));
		System.out.println(set.contains(16));		
		for(Integer intSet : set) {
			System.out.println(intSet);
		}
		set.clear();
		
		System.out.println("LIST");
		//Queue = liste (array en pas moche) avec doublons
		List<Integer> list = new ArrayList<Integer>();
		list.add(23);
		list.add(15);
		list.add(19);
		set.add(19);
		
		System.out.println(list.contains(15));
		System.out.println(list.contains(16));		
		for(Integer intList : list) {
			System.out.println(intList);
		}
		list.clear();
		
		System.out.println("MAP");
		//Queue = cl�/valeur
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("nbVoitures", 5);
		map.put("nbAvions", 1);
		map.put("nbBateaux", 7);
		map.put("nbBateaux", 7);
		
		String[] cleATest = {"test", "nbVoitures", "nbAvions", "nbAvion"};
		for (String cleTest : cleATest) {
			if(map.containsKey(cleTest)) {System.out.println(cleTest + " : " + map.get(cleTest));}
			else {System.out.println(cleTest + ": none");}
		}
		for (String mapKey : map.keySet()) {
			System.out.println(mapKey);
		}
		map.clear();
		
		System.out.println("QUEUE");
		//Queue = pile int�r�t = faire une liste d'attente, une pile...
		Queue<Integer> queue = new LinkedList<Integer>();
		System.out.println(queue.peek());
		queue.add(23);
		queue.add(15);
		queue.add(15);
		System.out.println(queue.peek());
		queue.add(19);
		System.out.println(queue.peek());
		for(Integer intQueue : queue) {
			System.out.println(intQueue);
		}
		queue.clear();
	}
}

package td_collections;

import java.util.*;

public class Joueur {
	
	// attributs d'un joueur
	private String nom;
	private LinkedList<Carte> main = new LinkedList<Carte>();
	private Carte derniereCarteJouee;
	
	// constructeur
	public Joueur(String nom){
		this.nom = nom;
	}
	
	// le joueur ramasse la carte et l'ajoute en dessous des cartes d�j� existantes dans la main
	public void ramasserCarte(Carte carte){
		this.main.add(carte);
	}

	// le joueur joue le premier �l�ment de sa main qui constitue donc sa derni�re carte jou�e.  
	public Carte jouerCarte(){
		this.derniereCarteJouee = main.peek();
		return main.pop();
	}
	// getter de derniereCarteJouee;
	public Carte getDerniereCarteJouee() {
		return derniereCarteJouee;
	}
	
	// le joueur gagne s'il a toutes les cartes dans da main.
	public boolean isWinner(){
		if(main.size() == 32) {return true;}
		return false;
	}
	
	// getter de main
	public LinkedList<Carte> getMain(){
		return main;
	}
	
	// getter du nom
	public String getNom(){
		return nom;
	}
	
	// Descrit compl�tement le joueur
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("\n ******************************************* \n");		
		sb.append(nom + " a  " +  main.size() + " cartes dans sa main \n");
		sb.append(main);
		sb.append("\n ******************************************* \n");
		return sb.toString();
	}
	
	
}

package td_collections;

import java.util.*;
public class Partie {

	// attributs d'une PArtie
	private ArrayList<Joueur> listJ = new ArrayList<Joueur>();
	private JeuCartes jeu;
	

	// constructeur de Partie
	public Partie(){
		this.jeu = new JeuCartes();
	}

	// ajout d'un joueur � la liste des joueurs
	public void ajouterUnJoueur(Joueur joueur){
		this.listJ.add(joueur);
	}

    // On m�lange le jeu.
	// On distribue les cartes aux joueurs 
	public void distribuerCartes(){
		this.jeu.melanger();
		while(!this.jeu.estVide()) {
			Iterator<Joueur> itrJ = listJ.listIterator();
			while(itrJ.hasNext()) {
				itrJ.next().ramasserCarte(jeu.distribuerUneCarte());
			}
		}
		
	}
	
	// Chaque joueur jette une carte sur la table.
	// On ne teste pas le cas o� les joueurs jettent une carte de m�me valeur.
	public ArrayList<Carte> jouerVosCartes(){
		ArrayList<Carte> listeCartesJouees = new ArrayList<Carte>();
		for(Joueur J : listJ) {
			listeCartesJouees.add(J.jouerCarte());
		}
		return listeCartesJouees;
	}
	
	// Parmis les cartes jou�es, laquelle est gagnante?
	public Carte carteGagnante(List<Carte> cartes) {
		Carte meilleureCarte = cartes.get(0);
		Iterator<Carte> it = cartes.iterator();
		while(it.hasNext()) {
			Carte c = it.next();
			if (c.getValeur().ordinal() > meilleureCarte.getValeur().ordinal()) {
				meilleureCarte = c;
			}
		}		
		return meilleureCarte;
	}
	
	// Quel joueur a jou� la carte gagnante?
	// On prend le premier joueur.
	public Joueur joueurGagnant(Carte carte) {
		for(Joueur J : listJ) {
			if(J.getDerniereCarteJouee() == carte) {return J;}
		}
		return null;
	}
	
	// Le gagnant ramasse toutes les cartes jouees 
    public void recupererCartesJouees(Joueur j, ArrayList<Carte> cartesJouees) {
    	for(Carte carte : cartesJouees) {
    		j.ramasserCarte(carte);
    	}
    }
    	
	
	public boolean partieTerminee() {
		boolean fin =false;
		Iterator<Joueur> it =listJ.iterator();
		while (it.hasNext() && fin == false) {
			Joueur jj = it.next();
			fin=jj.isWinner();
		}
		return fin;
		
	}

	public String toString(){
		return listJ.toString();
	}
				
	public static void main(String[] args) {

		// cr�ation d'une partie de Bataille
		Partie bataille = new Partie();
		
		// cr�ation de deux joueurs 
		Joueur Karadoc = new Joueur("Karadoc");
		Joueur Perceval = new Joueur("Perceval");
		
		// on ajoute les 2 joueurs � la partie
		bataille.ajouterUnJoueur(Karadoc);
		bataille.ajouterUnJoueur(Perceval);
		
		// on affiche le jeu de cartes
		System.out.println(bataille.jeu);
		
		// on distribue les cartes � l'ensemble des joueurs
		bataille.distribuerCartes();

		System.out.println(bataille.listJ.get(0));
		System.out.println(bataille.listJ.get(1));
		
		Scanner kbhit = new Scanner(System.in);
		boolean onContinue = true;
		while (bataille.partieTerminee()==false && onContinue) {
			ArrayList<Carte> lesCartesJouees =  bataille.jouerVosCartes();
			Carte laCarteGagnante                 =  bataille.carteGagnante(lesCartesJouees);
			Joueur leGagnant                         =  bataille.joueurGagnant(laCarteGagnante);
			bataille.recupererCartesJouees(leGagnant, lesCartesJouees);		
			System.out.println(bataille);

			System.out.println("Touche c pour continuer");
			String str = kbhit.next();  
			onContinue=str.equals("c") ? true : false;
		}
		
	}
}
		
 
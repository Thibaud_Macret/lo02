package td_collections;
import java.util.*;

public class JeuCartes {
	
	// attributs d'un jeu de cartes
	private LinkedList<Carte> tasCartes;
	public static final int nbrCartes = Valeur.values().length * Couleur.values().length; // ici 32 cartes
	
	// constructeur du jeu de cartes
	public JeuCartes(){
		tasCartes = new LinkedList<Carte>();
		Valeur[] v=Valeur.values(); // values renvoie un tableau de Valeur
		Couleur[] c=Couleur.values();
		for(int i=0 ; i < v.length; i++){
			for(int j=0 ; j < c.length ; j++){
				Carte carte = new Carte(v[i] , c[j] );
				tasCartes.add(carte);
			}
		}
	}
	
	// retire la premi�re carte du tas de cartes
	public Carte distribuerUneCarte(){ 
		return tasCartes.pop();
	}
	
	// M�lange de toutes les cartes. Appel de la m�thode statique shuffle de la classe Collections
	public void melanger(){
		Collections.shuffle(tasCartes);
	}
	
	
	// le tas de cartes est-il vide?
	public boolean estVide() {
		if(tasCartes.size() == 0) {return true;}
		return false;
	}
	
	public String toString(){
		return tasCartes.toString();
	}
	
	// getter du tas de cartes
	public LinkedList<Carte> getTasCartes(){
		return tasCartes;
	}
}

public interface Orientable {
	public void tourne_gauche();
	public void tourne_droite();
}

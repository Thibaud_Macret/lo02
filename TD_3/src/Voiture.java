public class Voiture extends Vehicule implements Pilotable, Orientable{
	@Override
	public void rouler() {
		System.out.println("Vroom vroom ça roule, attention aux petites filles !");
		this.roule = true;
		while (this.roule && this.essence > 0) {
			this.kilometres+=vitesse;
			this.essence -= this.consommation;
		}
	}
	
	@Override
	public void rouler(int distance) {
		System.out.println("Vroom vroom ça roule sur " + distance + " kilomètres");
		this.kilometres += distance;
		this.essence -= this.consommation*distance;
	}

	@Override
	public void accelerer(double delta) {
		vitesse += delta;
		
	}

	@Override
	public void ralentir(double delta) {
		vitesse -= delta;
		
	}

	@Override
	public void tourne_gauche() {
		System.out.println("à gauche !");
		
	}

	@Override
	public void tourne_droite() {
		System.out.println("à droite (comme Zemzem)!");
		
	}
	
	
}

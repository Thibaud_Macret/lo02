public class main {
	 public static void main(String[] args) {
		Vehicule[] vehicules = new Vehicule[2];
		Voiture voiture = new Voiture();
		Train train = new Train();
		vehicules[0] = voiture;
		vehicules[1] = train;
		
		train.rouler();
		for (int i = 0; i < vehicules.length; i++) {
			vehicules[i].rouler(150);
		}
		voiture.tourne_droite();
	}
}

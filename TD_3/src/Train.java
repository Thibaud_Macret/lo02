public class Train extends Vehicule implements Pilotable{
	@Override
	public void rouler() {
		System.out.println("Tchou tchou !");
		this.roule = true;
		while (this.roule && this.essence > 0) {
			this.kilometres+=vitesse;
			this.essence -= this.consommation;
			System.out.println("Tchou tchou !" + this.kilometres + " km parcourus et reste " + this.essence);
		}
	}

	@Override
	public void rouler(int distance) {
		if(this.essence > this.consommation*distance)
		{
			System.out.println("Tchou thou sur " + distance + " kilomètres");
			this.kilometres += distance;
			this.essence -= this.consommation*distance;			
		} else {
			System.out.println("pas assez d'essence, retard SNCFed");
		}
	}
	
	@Override
	public void accelerer(double delta) {
		vitesse += delta;
		
	}

	@Override
	public void ralentir(double delta) {
		vitesse -= delta;
		
	}
}

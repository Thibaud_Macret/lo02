public abstract class Vehicule {
 
	public final int capaciteReservoir = 50;
	public final double consommation = 0.1;
	protected int kilometres;
	protected float essence;
	protected boolean roule;
	protected double vitesse = 1;
	
	public Vehicule(int kilometres) {
		this.essence = capaciteReservoir;
		this.kilometres = kilometres;
		this.roule = false;
		}
		
	public Vehicule () {
		this.essence = capaciteReservoir;
		this.kilometres = 0;
		this.roule = false;
	}
	 
	public abstract void rouler();
	public abstract void rouler(int distance);
	
	public void stopper() {
		this.roule = false;
	}
}
public interface Pilotable {
	public void accelerer(double delta);
	public void ralentir(double delta);
}

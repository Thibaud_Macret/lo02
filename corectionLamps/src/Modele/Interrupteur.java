package Modele;
public class Interrupteur {
	
		private Commutateur commutateur;
		
		public Interrupteur () {
		commutateur = new Commutateur(5);
		}
		
		public void appuyer() {
		commutateur.commuter();
		}
		
		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append("Interrupteur vers ");
			sb.append(commutateur);
			return sb.toString();
		}
		
		public Commutateur getCommutateur(){
			return commutateur;
		}
}

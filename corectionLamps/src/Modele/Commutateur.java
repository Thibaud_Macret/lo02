package Modele;
import java.util.Observable;

public class Commutateur extends Observable {
	
private Lampe[] lampes;
private int etat;
private int nombreEtats;

public Commutateur(int nombreLampes){
	etat=0;
	nombreEtats = nombreLampes*2;
	lampes=new Lampe[nombreLampes];
	
	Lampe lampeFaible = new Lampe(Lampe.PUISSANCE_STANDARD / 2, 0);
	lampes[0]=lampeFaible;
	
	for (int i=1 ; i < nombreLampes ; i++){
		Lampe lampeStandard = new Lampe(Lampe.PUISSANCE_STANDARD, i);
		lampes[i]=lampeStandard;
	}
}

public void commuter(){
	etat = (etat + 1) % nombreEtats;

	this.setChanged();
	this.notifyObservers();
	
	int puissanceRequise = etat * Lampe.PUISSANCE_STANDARD /2;
	
	// Allumage/eteignage de la lampe faible
	Lampe lampeFaible = lampes[0];
	if ((puissanceRequise % Lampe.PUISSANCE_STANDARD) != 0) {
			lampeFaible.allumer();
		} else {
			lampeFaible.eteindre();
			}
	
	// Allumage/Eteignage des autres lampes
	for (int i = 1; i <= (puissanceRequise / Lampe.PUISSANCE_STANDARD); i++) {
			Lampe lampe = lampes[i];
			lampe.allumer();
	}
	
	for (int i = Math.max((puissanceRequise / Lampe.PUISSANCE_STANDARD) + 1, 1); i < lampes.length; i++) {
			Lampe lampe = lampes[i];
			lampe.eteindre();
	}			
				

}

public int getEtat (){
	return etat;
}

public Lampe[] getLampes(){
	return lampes;
}

public String toString() {
	StringBuffer sb = new StringBuffer();
	sb.append("Commutateur (");
	sb.append("Etat : " + etat);
	sb.append(")\n");
//	for (int i = 0; i < lampes.length; i++) {
//		sb.append(lampes[i]);
//	}
	return sb.toString();
}

}

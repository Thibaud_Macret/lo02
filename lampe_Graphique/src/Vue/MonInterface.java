package Vue;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JLabel;

import Controleur.ControleurInterupteur;
import Modele.Interrupteur;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MonInterface {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MonInterface window = new MonInterface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MonInterface() {
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JCheckBox chckbxLamp1 = new JCheckBox("Lampe1");
		chckbxLamp1.setBounds(36, 52, 97, 23);
		frame.getContentPane().add(chckbxLamp1);
		
		JCheckBox chckbxLamp2 = new JCheckBox("Lamp2");
		chckbxLamp2.setBounds(36, 129, 97, 23);
		frame.getContentPane().add(chckbxLamp2);
		
		JCheckBox chckbxLamp3 = new JCheckBox("Lamp3");
		chckbxLamp3.setBounds(36, 214, 97, 23);
		frame.getContentPane().add(chckbxLamp3);
		
		JButton btnNewButton = new JButton("LE BOUTON");
		Interrupteur interupt = new Interrupteur();
		ControleurInterupteur ctrlI= new ControleurInterupteur(btnNewButton, interupt);
		btnNewButton.setBounds(288, 129, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Etat");
		lblNewLabel.setBounds(178, 133, 46, 14);
		frame.getContentPane().add(lblNewLabel);
	}
}

package Controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import Modele.Interrupteur;

public class ControleurInterupteur {
	
	private JButton rpzGraphique;
	private Interrupteur rpzModele;
	
	public ControleurInterupteur(JButton rpzGraphique, Interrupteur rpzModele) {
		this.rpzGraphique = rpzGraphique;
		this.rpzModele = rpzModele;
		
		rpzGraphique.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.print("click");
				rpzModele.appuyer();
			}
		});
	}
}

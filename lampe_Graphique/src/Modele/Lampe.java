package Modele;
import java.util.Observable;

public class Lampe extends Observable {

	public final static int PUISSANCE_STANDARD = 100;
	
	private boolean allumee;
    private int puissance; 
    private int numero;

public Lampe(int puissance, int num){
	this.puissance = puissance;
	numero=num;
	allumee = false;
}

public int getNumero(){
	return numero;
}
public int getPuissance(){
	return puissance;
}

public void allumer (){
	//System.out.println("Allum�e");
	this.allumee=true;
	this.setChanged();
	this.notifyObservers();
}

public void eteindre (){
	//System.out.println("Eteinte");
	this.allumee=false;
	this.setChanged();
	this.notifyObservers();
}

public boolean isAllumee(){
	return allumee;
}

@Override
public String toString() {
	return "Lampe [puissance="+puissance+" ; allumee=" + allumee + "]\n";
}

}

